# -*- coding: utf-8 -*-
"""
/***************************************************************************
 TIRC_Tool
                                 A QGIS plugin
                              -------------------
        begin                : 2019-08-24
        email                : kumahl@gmail.com
 ***************************************************************************/
""" 
import os.path

from qgis.core import *
from qgis.gui import *

from osgeo import gdal, osr
import processing

#import zmq

from PyQt5.QtCore import QSettings, QTranslator, qVersion, QCoreApplication, QSize, QPoint, QSizeF
from PyQt5.QtGui import QIcon, QPixmap, QPainter, QPen, QScreen, QColor, QKeySequence, QFont, QTextDocument, QBrush, QTransform
from PyQt5.QtWidgets import QAction, QFileDialog, QMessageBox, QLabel, QColorDialog, QWidget, QShortcut, QApplication, QTableWidgetItem, QHeaderView, QGraphicsView, QGraphicsScene, QGraphicsPixmapItem, QFrame, QGraphicsEllipseItem, QGraphicsSimpleTextItem, QDialog


# Initialize Qt resources from file resources.py
from .resources import *
# Import the code for the dialog
from .TIRC_Tool_dialog import TIRC_ToolDialog
#import TIRC_ToolGUI


order = 0


class TIRC_Tool:
    """QGIS Plugin Implementation."""

    def __init__(self, iface):
        """Constructor.

        :param iface: An interface instance that will be passed to this class
            which provides the hook by which you can manipulate the QGIS
            application at run time.
        :type iface: QgsInterface
        """
        # Save reference to the QGIS interface
        self.iface = iface
        self.canvas = iface.mapCanvas()
        # initialize plugin directory
        self.plugin_dir = os.path.dirname(__file__)
        # initialize locale
        locale = QSettings().value('locale/userLocale')[0:2]
        locale_path = os.path.join(
            self.plugin_dir,
            'i18n',
            'TIRC_Tool_{}.qm'.format(locale))

        if os.path.exists(locale_path):
            self.translator = QTranslator()
            self.translator.load(locale_path)

            if qVersion() > '4.3.3':
                QCoreApplication.installTranslator(self.translator)
        # Create the dialog (after translation) and keep reference
        self.dlg = TIRC_ToolDialog()
        # Declare instance attributes
        self.actions = []
        #self.menu = self.tr(u'&TIRC_Tool')
        self.menu = self.tr(u'&__TIRC_Tool__')
        # TODO: We are going to let the user set this up in a future iteration
        #self.toolbar = self.iface.addToolBar(u'TIRC_Tool')
        #self.toolbar.setObjectName(u'TIRC_Tool')
        self.toolbar = self.iface.addToolBar(u'__TIRC_Tool__')
        self.toolbar.setObjectName(u'__TIRC_Tool__')


        icon_6  = QgsApplication.qgisSettingsDirPath() + 'python\\plugins\\TIRC_Tool\\icon\\saveFile.png'
        icon_7  = QgsApplication.qgisSettingsDirPath() + 'python\\plugins\\TIRC_Tool\\icon\\redraw.svg'
        self.dlg.pushButton_6.setIcon(QIcon(icon_6))
        self.dlg.pushButton_7.setIcon(QIcon(icon_7))



    # noinspection PyMethodMayBeStatic
    def tr(self, message):
        """Get the translation for a string using Qt translation API.

        We implement this ourselves since we do not inherit QObject.

        :param message: String for translation.
        :type message: str, QString

        :returns: Translated version of message.
        :rtype: QString
        """
        # noinspection PyTypeChecker,PyArgumentList,PyCallByClass
        return QCoreApplication.translate('TIRC_Tool', message)

    def up(self):
        QMessageBox.warning(self.iface.mainWindow(), u"警告", u"up", QMessageBox.Cancel)

    def loadXYZ(self):
        urlWithParams = 'type=xyz&url=https://rs.happyman.idv.tw/map/moi_osm/{z}/{x}/{y}.pngg&zmax=19&zmin=0&crs=EPSG3857'
        rasterLyr = QgsRasterLayer(urlWithParams, 'name', "wms")
        QgsProject.instance().addMapLayer(rasterLyr)


        
        #loadXYZ(urlWithParams, 'OpenStreetMap')



    def loadWMTS(self):
        #urlWithParams = "url=crs=EPSG:4326&format=image/png&layers=tasmania&styles&url=https://demo.geo-solutions.it/geoserver/ows"
        #urlWithParams = "contextualWMSLegend=0&crs=EPSG:3857&dpiMode=7&featureCount=10&format=image/jpeg&layers=PHOTO2&styles=default&tileMatrixSet=GoogleMapsCompatible&url=http://maps.nlsc.gov.tw/S_Maps/wmts"
        #urlWithParams = 'http://irs.gis-lab.info/?layers=landsat&styles=&format=image/jpeg&crs=EPSG:4326'
        self.urlWithParams = "url=https://demo.geo-solutions.it/geoserver/ows&format=image/png&layers=tasmania&styles=&crs=EPSG:4326"
        #self.rlayer = QgsRasterLayer("https://www.google.com/logos/doodles/2020/thank-you-custodial-and-sanitation-workers-6753651837108756-law.gif", "test")
        self.rlayer = QgsRasterLayer(self.urlWithParams, 'some layer name', 'wms')
        
        #self.rlayer = QgsRasterLayer("D:\\DATA\\jpg\\S__22274051.jpg", "111")
        #self.vlayer = QgsVectorLayer("D:\\DATA\\shp\\tw_county_97.shp", "layer_name", 'ogr')
        if not self.rlayer.isValid():
            #print("Layer failed to load!")
            QMessageBox.warning(self.iface.mainWindow(), u"警告", u"Layer failed to load!", QMessageBox.Cancel)


        
        self.dlg.graphicsView_2.setExtent(self.rlayer.extent())
        self.dlg.graphicsView_2.setLayers([self.rlayer])
        #self.canvas = QgsMapCanvas()
        #QMessageBox.warning(self.iface.mainWindow(), u"警告", u"11up11", QMessageBox.Cancel)

        #self.new_dialog = QDialog()
        #self.new_dialog.resize(800, 600)

        #self.map_canvas = QgsMapCanvas(self.new_dialog)
        #self.map_canvas.setGeometry(QtCore.QRect(400, 400, 300, 300))
        #self.map_canvas.setExtent(self.rlayer.extent())
        #self.map_canvas.setLayers([self.rlayer])
        

        #self.new_dialog.show()


        #self.canvas = QgsMapCanvas(self.dlg)
        #self.canvas.setGeometry(QtCore.QRect(60, 57, 113, 20))
        #self.canvas.setExtent(self.rlayer.extent())
        #self.canvas.setLayers([self.rlayer])
        #QgsProject.instance().addMapLayer(self.rlayer)
        
        #self.dlg.graphicsView_2 = QgsMapCanvas(self.dlg)
        #self.dlg.graphicsView_2.setGeometry(QtCore.QRect(100, 100, 100, 100))
        #self.dlg.graphicsView_2.setExtent(self.rlayer.extent())
        #self.dlg.graphicsView_2.setLayers([self.rlayer])
        
        #self.canvas.show()
        #QMessageBox.warning(self.iface.mainWindow(), u"警告", u"22up22", QMessageBox.Cancel)
        #self.run()

    def add_action(
        self,
        icon_path,
        text,
        callback,
        enabled_flag=True,
        add_to_menu=True,
        add_to_toolbar=True,
        status_tip=None,
        whats_this=None,
        parent=None):
        """Add a toolbar icon to the toolbar.

        :param icon_path: Path to the icon for this action. Can be a resource
            path (e.g. ':/plugins/foo/bar.png') or a normal file system path.
        :type icon_path: str

        :param text: Text that should be shown in menu items for this action.
        :type text: str

        :param callback: Function to be called when the action is triggered.
        :type callback: function

        :param enabled_flag: A flag indicating if the action should be enabled
            by default. Defaults to True.
        :type enabled_flag: bool

        :param add_to_menu: Flag indicating whether the action should also
            be added to the menu. Defaults to True.
        :type add_to_menu: bool

        :param add_to_toolbar: Flag indicating whether the action should also
            be added to the toolbar. Defaults to True.
        :type add_to_toolbar: bool

        :param status_tip: Optional text to show in a popup when mouse pointer
            hovers over the action.
        :type status_tip: str

        :param parent: Parent widget for the new action. Defaults None.
        :type parent: QWidget

        :param whats_this: Optional text to show in the status bar when the
            mouse pointer hovers over the action.

        :returns: The action that was created. Note that the action is also
            added to self.actions list.
        :rtype: QAction
        """

        icon = QIcon(icon_path)
        action = QAction(icon, text, parent)
        action.triggered.connect(callback)
        action.setEnabled(enabled_flag)

        if status_tip is not None:
            action.setStatusTip(status_tip)

        if whats_this is not None:
            action.setWhatsThis(whats_this)

        if add_to_toolbar:
            self.toolbar.addAction(action)

        if add_to_menu:
            self.iface.addPluginToVectorMenu(
                self.menu,
                action)

        self.actions.append(action)

        return action

    def initGui(self):
        """Create the menu entries and toolbar icons inside the QGIS GUI."""

        icon_path = ''
        self.add_action(
            icon_path,
            text=self.tr(u'__TIRC_Tool__'),
            callback=self.run,
            parent=self.iface.mainWindow())

        

        self.dlg.pushButton.clicked.connect(self.up)
        self.dlg.pushButton_2.clicked.connect(self.loadWMTS)
        self.dlg.pushButton_3.clicked.connect(self.loadXYZ)

        #self.shortcut = QShortcut(QKeySequence("Ctrl+Z"), self.dlg)
        #self.shortcut.activated.connect(self.undo)        

    def unload(self):
        """Removes the plugin menu item and icon from QGIS GUI."""
        for action in self.actions:
            self.iface.removePluginVectorMenu(
                self.tr(u'&__TIRC_Tool__'),
                action)
            self.iface.removeToolBarIcon(action)
        # remove the toolbar
        del self.toolbar


    def run(self):
        self.dlg.graphicsView_2 = QgsMapCanvas(self.dlg)
        self.dlg.graphicsView_2.setGeometry(QtCore.QRect(340, 610, 256, 192))

 
        

        """Run method that performs all the real work"""
        self.dlg.show()
        # Run the dialog event loop
        self.dlg.exec_()




